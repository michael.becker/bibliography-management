/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 *
 * @param {string} entryId
 *
 * @return {string} the Base64 encoded content of the PDF document
 */
function loadPDFContent(entryId) {
}

/**
 * Displays the given PDF content on the given element. In doing so, canvas-subelements are being created as children of the given element.
 * @param {string} pdfContent the Base64-encoded PDF content
 * @param {HTMLElement} element the element where to add the content
 */
function displayPDF(pdfContent, element) {
    const pdfJS = window['pdfjs-dist/build/pdf'];
    pdfJS.GlobalWorkerOptions.workerSrc = "http://mozilla.github.io/pdf.js/build/pdf.worker.js";

    const loadingTask = pdfJS.getDocument({data: pdfContent});
    loadingTask.promise.then(function (pdfDocument) {
        const pages = pdfDocument.numPages;

        for (let page = 1; page < pages; page++) {
            const pageCanvas = document.createElement("canvas");
            pageCanvas.id = "pdf_page_" + page;
            element.insertAdjacentElement("beforeend", pageCanvas);

            pdfDocument.getPage(page).then(function (pdfPage) {
                const pageCanvas = document.getElementById("pdf_page_" + pdfPage.pageNumber);
                const scale = 1.5;
                const viewport = pdfPage.getViewport({scale: scale});

                // Prepare canvas using PDF page dimensions
                const context = pageCanvas.getContext('2d');
                pageCanvas.height = viewport.height;
                pageCanvas.width = viewport.width;

                // Render PDF page into canvas context
                const renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };

                const renderTask = pdfPage.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
            })
        }
    });
}