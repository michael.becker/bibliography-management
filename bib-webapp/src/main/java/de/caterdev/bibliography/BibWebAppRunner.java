package de.caterdev.bibliography;

import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.model.database.BibUser;
import de.caterdev.bibliography.model.database.DatabaseACL;
import de.caterdev.bibliography.model.database.DatabaseRights;
import de.caterdev.bibliography.model.entry.Article;
import de.caterdev.bibliography.model.entry.Book;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.BibUserRepository;
import de.caterdev.bibliography.repo.DatabaseACLRepository;
import de.caterdev.bibliography.repo.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Year;
import java.util.Arrays;
import java.util.Collections;

@Component
public class BibWebAppRunner implements ApplicationRunner {
    private final BibUserRepository userRepository;
    private final BibDatabaseRepository entryDatabaseRepository;
    private final EntryRepository entryRepository;
    private final DatabaseACLRepository aclRepository;

    @Autowired
    public BibWebAppRunner(BibUserRepository userRepository, BibDatabaseRepository entryDatabaseRepository, EntryRepository entryRepository, DatabaseACLRepository aclRepository) {
        this.userRepository = userRepository;
        this.entryDatabaseRepository = entryDatabaseRepository;
        this.entryRepository = entryRepository;
        this.aclRepository = aclRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        BibUser user = new BibUser();
        user.setUsername("username");
        user.setPassword(new BCryptPasswordEncoder().encode("password"));

        user = userRepository.save(user);

        BibDatabase entryDatabase = new BibDatabase();
        entryDatabase.setOwner(user);
        entryDatabase.setName("Literatur-DB");
        entryDatabase = entryDatabaseRepository.save(entryDatabase);

        Article article = new Article();
        article.setAuthors(Collections.singletonList("Michael Becker"));
        article.setTitle("Deine Mutter");
        article.setJournal("Journal of Angewandter Mutterwitz");
        article.setYear(Year.now());
        article.setUrl("https://www.caterdev.de");
        article.setDoi("10.1007/s13280-019-01299-3");
        article.setAbstrct("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor nibh in suscipit fermentum. Curabitur eu diam sodales, facilisis ipsum vel, placerat urna.");
        article = entryRepository.save(article);

        Book book = new Book();
        book.setAuthors(Arrays.asList("Michael Becker", "Stephan Klingner", "Julia Friedrich"));
        book.setTitle("Unsere Mütter");
        book.setPublisher("Unser toller Verlag");
        book.setYear(Year.parse("1996"));
        book.setUrl("https://infai.org");
        book.setDoi("10.1063/1.1461333");
        book.setAbstrct("Comments are held for moderation by Physics Today staff. Off-topic statements and personal attacks will not be approved.");
        book = entryRepository.save(book);

        entryDatabase.addEntries(article, book);
        entryDatabaseRepository.save(entryDatabase);

        BibUser user2 = new BibUser();
        user2.setUsername("user2");
        user2.setPassword("password");
        user2 = userRepository.save(user2);
        BibDatabase database = new BibDatabase();
        database.setName("shared db from user2");
        database = entryDatabaseRepository.save(database);

        DatabaseACL acl = new DatabaseACL();
        acl.setDatabase(database);
        acl.setUser(user);
        acl.setDatabaseRights(DatabaseRights.Write);
        aclRepository.save(acl);
    }
}
