package de.caterdev.bibliography.storage;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryFile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;

@Service
public class FileStorage {
    private final Path rootLocation;

    public FileStorage() {
        this.rootLocation = Paths.get("target/upload-dir");
    }

    public void save(UUID entryId, MultipartFile file) {
        if (!file.isEmpty()) {
            String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
            try {
                InputStream inputStream = file.getInputStream();
                Path dir = rootLocation.resolve(entryId.toString() + "/");
                Path targetFile = rootLocation.resolve(entryId.toString() + "/" + filename);

                if (!Files.isDirectory(dir)) {
                    Files.createDirectories(dir);
                }
                Files.copy(inputStream, targetFile, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Resource load(EntryFile entryFile) throws MalformedURLException {
        Path file = rootLocation.resolve(entryFile.getEntry().getId() + "/" + entryFile.getFilename());

        return new UrlResource(file.toUri());
    }

    @Deprecated
    public Resource load(Entry entry) throws MalformedURLException {
        Path file = rootLocation.resolve(entry.getId() + "/" + entry.getFilename());

        return new UrlResource(file.toUri());
    }
}
