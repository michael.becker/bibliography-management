package de.caterdev.bibliography.controller;

import de.caterdev.bibliography.form.EntryForm;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.EntryRepository;
import de.caterdev.bibliography.storage.FileStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.net.MalformedURLException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/databases/{databaseId}/entries")
public class EntryController {
    private final BibDatabaseRepository entryDatabaseRepository;
    private final EntryRepository entryRepository;
    private final FileStorage fileStorage;

    @Autowired
    public EntryController(BibDatabaseRepository entryDatabaseRepository, EntryRepository entryRepository, FileStorage fileStorage) {
        this.entryDatabaseRepository = entryDatabaseRepository;
        this.entryRepository = entryRepository;
        this.fileStorage = fileStorage;
    }

    @GetMapping("/list")
    public String list(@PathVariable UUID databaseId, Model model) {
        BibDatabase database = entryDatabase(databaseId);
        List<Entry> entries = database.getEntries();

        model.addAttribute("entries", EntryForm.fromEntries(entries));
        model.addAttribute("types", EntryType.values());

        return "listEntries";
    }

    @GetMapping("/create/{type}")
    public String create(@PathVariable String type, Model model) {
        EntryForm entryForm = EntryForm.getInstance(EntryType.valueOf(type));

        model.addAttribute("entry", entryForm);
        model.addAttribute("types", EntryType.values());

        return "editEntry";
    }

    @GetMapping("/{id}/file")
    public ResponseEntity<Resource> file(@PathVariable UUID id) throws MalformedURLException {
        Resource file = fileStorage.load(entryRepository.findById(id).orElseThrow());

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable UUID id, Model model) {
        Entry entry = entryRepository.findById(id).orElseThrow();
        model.addAttribute("entry", EntryForm.fromEntry(entry));
        model.addAttribute("types", EntryType.values());

        return "editEntry";
    }


    @PostMapping("/update")
    public String update(@PathVariable("databaseId") UUID databaseId, @ModelAttribute EntryForm entryForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            return null;
        } else {
            System.out.println("searching for db with id "+databaseId);
            System.out.println("result: "+entryDatabaseRepository.findById(databaseId).get());
            Entry result = updateEntry(entryForm, entryDatabaseRepository.findById(databaseId).get());
            System.out.println("saved " + result);
        }

        return "redirect:/databases/{databaseId}/entries/list";
    }

    @Transactional
    protected Entry updateEntry(EntryForm entryForm, BibDatabase entryDatabase) {
        Entry entry = entryRepository.save(entryForm.toEntry());
        entryDatabase.addEntries(entry);
        entryDatabaseRepository.save(entryDatabase);
        fileStorage.save(entry.getId(), entryForm.getFile());

        return entry;
    }

    @ModelAttribute("database")
    public BibDatabase entryDatabase(@PathVariable("databaseId") UUID databaseId) {
        return entryDatabaseRepository.getOne(databaseId);
    }
}
