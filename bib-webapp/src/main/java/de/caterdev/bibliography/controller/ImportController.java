package de.caterdev.bibliography.controller;

import de.caterdev.bibliography.form.ImportForm;
import de.caterdev.bibliography.interfce.InterfaceType;
import de.caterdev.bibliography.interfce.ris.RISImporter;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.UUID;

@Controller
@RequestMapping("databases/{databaseId}/import")
public class ImportController {
    private final RISImporter risImporter;
    private final EntryRepository entryRepository;
    private final BibDatabaseRepository entryDatabaseRepository;

    @Autowired
    public ImportController(RISImporter risImporter, EntryRepository entryRepository, BibDatabaseRepository entryDatabaseRepository) {
        this.risImporter = risImporter;
        this.entryRepository = entryRepository;
        this.entryDatabaseRepository = entryDatabaseRepository;
    }

    @GetMapping("/ris")
    public String importRIS(Model model) {
        ImportForm importForm = new ImportForm();
        importForm.setType(InterfaceType.RIS);

        model.addAttribute("importForm", importForm);
        return "importRIS";
    }

    @PostMapping("/ris")
    @Transactional
    public String doImport(@PathVariable("databaseId") UUID databaseId, @ModelAttribute ImportForm importForm, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            return null;
        }

        String content = importForm.getContent();
        MultipartFile file = importForm.getFile();

        if ((null == content || content.equals("")) && file.isEmpty()) {
            throw new RuntimeException("One of content or file must be set!");
        } else if (!file.isEmpty()) {
            content = new String(file.getBytes());
        }

        if (null == content) {
            throw new RuntimeException("Content after parsing still null!");
        }

        Entry entry = switch (importForm.getType()) {
            case RIS -> risImporter.parse(content);
            default -> throw new RuntimeException("Type " + importForm.getType() + " not supported");
        };


        entry = entryRepository.save(entry);
        BibDatabase database = entryDatabase(databaseId);
        database.addEntries(entry);
        entryDatabaseRepository.save(database);

        return "redirect:/databases/{databaseId}/entries/list";
    }

    @ModelAttribute("database")
    public BibDatabase entryDatabase(@PathVariable("databaseId") UUID databaseId) {
        return entryDatabaseRepository.getOne(databaseId);
    }
}
