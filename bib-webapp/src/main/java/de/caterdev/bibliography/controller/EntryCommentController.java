/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.controller;

import de.caterdev.bibliography.form.EntryCommentForm;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryComment;
import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.model.database.BibUser;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.EntryCommentRepository;
import de.caterdev.bibliography.repo.EntryRepository;
import de.caterdev.bibliography.storage.FileStorage;
import de.caterdev.bibliography.usermgmt.BibUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/databases/{databaseId}/entries/{entryId}/comments")
public class EntryCommentController {
    private final BibDatabaseRepository databaseRepository;
    private final EntryRepository entryRepository;
    private final EntryCommentRepository entryCommentRepository;
    private final BibUserService userService;
    private final FileStorage fileStorage;

    @Autowired
    public EntryCommentController(BibDatabaseRepository databaseRepository, EntryRepository entryRepository, EntryCommentRepository entryCommentRepository, BibUserService userService, FileStorage fileStorage) {
        this.databaseRepository = databaseRepository;
        this.entryRepository = entryRepository;
        this.entryCommentRepository = entryCommentRepository;
        this.userService = userService;
        this.fileStorage = fileStorage;
    }

    @GetMapping
    public String showComments(@PathVariable UUID entryId, Model model, Authentication authentication) throws Exception {
        BibUser user = userService.getUser(authentication);
        Entry entry = entryRepository.getOne(entryId);
        Optional<EntryComment> commentOpt = entryCommentRepository.findByUserAndEntry(user, entry);
        EntryComment comment = commentOpt.orElse(new EntryComment(user, entry));
        String pdfContent = Base64.getEncoder().encodeToString(fileStorage.load(entry).getInputStream().readAllBytes());

        model.addAttribute("entryComment", EntryCommentForm.from(comment));
        model.addAttribute("pdfContent", pdfContent);

        return "commentEntry";
    }

    @PostMapping
    public String updateComments(@PathVariable UUID entryId, @ModelAttribute EntryCommentForm entryCommentForm, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            return null;
        }

        BibUser user = userService.getUser(authentication);
        Optional<EntryComment> commentOpt = entryCommentRepository.findById(entryCommentForm.getId());
        EntryComment comment = commentOpt.orElse(new EntryComment(user, entryRepository.getOne(entryId)));
        comment.setComment(entryCommentForm.getComment());

        entryCommentRepository.save(comment);

        return "redirect:/databases/{databaseId}/entries/list";
    }

    @ModelAttribute("database")
    public BibDatabase entryDatabase(@PathVariable("databaseId") UUID databaseId) {
        return databaseRepository.getOne(databaseId);
    }

    @ModelAttribute("entry")
    public Entry entry(@PathVariable("entryId") UUID entryId) {
        return entryRepository.getOne(entryId);
    }

}


