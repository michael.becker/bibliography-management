/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.controller;

import de.caterdev.bibliography.interfce.bib.BibExporter;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("databases/{databaseId}/export")
public class ExportController {
    private final BibExporter bibExporter;
    private final BibDatabaseRepository entryDatabaseRepository;

    @Autowired
    public ExportController(BibExporter bibExporter, EntryRepository entryRepository, BibDatabaseRepository entryDatabaseRepository) {
        this.bibExporter = bibExporter;
        this.entryDatabaseRepository = entryDatabaseRepository;
    }

    @GetMapping("/bib")
    public ResponseEntity<String> exportBib(@PathVariable("databaseId") UUID databaseId) {
        List<Entry> entries = entryDatabase(databaseId).getEntries();

        StringBuilder content = new StringBuilder();
        for (Entry entry : entries) {
            content.append(bibExporter.export(entry)).append("\n\n");
        }

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"library.bib\"").body(content.toString());
    }

    @ModelAttribute("database")
    public BibDatabase entryDatabase(@PathVariable("databaseId") UUID databaseId) {
        return entryDatabaseRepository.getOne(databaseId);
    }
}
