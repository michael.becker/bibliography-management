/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.controller;

import de.caterdev.bibliography.model.database.BibUser;
import de.caterdev.bibliography.repo.BibDatabaseRepository;
import de.caterdev.bibliography.repo.DatabaseACLRepository;
import de.caterdev.bibliography.usermgmt.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {
    private final BibDatabaseRepository entryDatabaseRepository;
    private final DatabaseACLRepository aclRepository;

    @Autowired
    public IndexController(BibDatabaseRepository entryDatabaseRepository, DatabaseACLRepository aclRepository) {
        this.entryDatabaseRepository = entryDatabaseRepository;
        this.aclRepository = aclRepository;
    }

    @GetMapping("/")
    public String index(Model model, Authentication authentication) {
        BibUser user = ((AuthUser) authentication.getPrincipal()).getBibUser();
        model.addAttribute("databases", entryDatabaseRepository.findAllByOwner(user));
        model.addAttribute("sharedDatabases", aclRepository.findAllByUser(user));

        return "dashboard";
    }
}
