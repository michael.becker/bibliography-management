package de.caterdev.bibliography.form;

import de.caterdev.bibliography.model.entry.Book;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookForm extends EntryForm {
    public BookForm() {
        setType(EntryType.Book);
    }

    public static EntryForm fromBook(Book book) {
        BookForm entryForm = initValues(book, new BookForm());

        entryForm.setAuthors(String.join(",", book.getAuthors()));
        entryForm.setTitle(book.getTitle());
        entryForm.setPublisher(book.getPublisher());
        entryForm.setYear(book.getYear());

        entryForm.setVolume(book.getVolume());
        entryForm.setSeries(book.getSeries());
        entryForm.setAddress(book.getAddress());
        entryForm.setEdition(book.getEdition());
        entryForm.setMonth(book.getMonth());
        entryForm.setNote(book.getNote());

        return entryForm;
    }

    public static Book toBook(EntryForm entryForm) {
        Book book = initValues(entryForm, new Book());

        book.setAuthors(Arrays.asList(entryForm.getAuthors().split(",")));
        book.setTitle(entryForm.getTitle());
        book.setPublisher(entryForm.getPublisher());
        book.setYear(entryForm.getYear());

        book.setVolume(entryForm.getVolume());
        book.setSeries(entryForm.getSeries());
        book.setAddress(entryForm.getAddress());
        book.setEdition(entryForm.getEdition());
        book.setMonth(entryForm.getMonth());
        book.setNote(entryForm.getNote());

        return book;
    }
}
