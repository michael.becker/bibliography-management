package de.caterdev.bibliography.form;

import de.caterdev.bibliography.model.*;
import de.caterdev.bibliography.model.entry.Article;
import de.caterdev.bibliography.model.entry.Book;
import de.caterdev.bibliography.model.entry.Booklet;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Data
public class EntryForm {
    private UUID id;
    private String authors;
    private String title;
    private String journal;
    private Year year;
    private String volume;
    private String number;
    private String pages;
    private Month month;
    private String note;
    private String publisher;
    private String series;
    private String address;
    private String edition;
    private String howPublished;
    private String abstrct;
    private String notes;
    private String bibtexSource;
    private String doi;
    private String url;
    private MultipartFile file;
    private String filename;
    private EntryType type;

    public static EntryForm getInstance(EntryType type) {
        return switch (type) {
            case Article -> new ArticleForm();
            case Book -> new BookForm();
            case Booklet -> new BookletForm();
            default -> throw new RuntimeException("Unsupported entry type " + type);
        };
    }

    public static EntryForm fromEntry(Entry entry) {
        return switch (entry.getType()) {
            case Article -> ArticleForm.fromArticle((Article) entry);
            case Book -> BookForm.fromBook((Book) entry);
            case Booklet -> BookletForm.fromBooklet((Booklet) entry);
            default -> throw new RuntimeException("Unsupported entry type " + entry.getType());
        };
    }

    public static Collection<EntryForm> fromEntries(Collection<Entry> entries) {
        Collection<EntryForm> entryForms = new ArrayList<>(entries.size());
        for (Entry entry : entries) {
            entryForms.add(EntryForm.fromEntry(entry));
        }

        return entryForms;
    }

    public static Entry toEntry(EntryForm entryForm) {
        return switch (entryForm.getType()) {
            case Article -> ArticleForm.toArticle(entryForm);
            case Book -> BookForm.toBook(entryForm);
            case Booklet -> BookletForm.toBooklet(entryForm);
            default -> throw new RuntimeException("Unsupported entry type " + entryForm.getType());
        };
    }

    public Entry toEntry() {
        return EntryForm.toEntry(this);
    }


    protected static <T extends EntryForm, U extends Entry> U initValues(T source, U target) {
        target.setId(source.getId());
        target.setUrl(source.getUrl());
        target.setNotes(source.getNotes());
        target.setAbstrct(source.getAbstrct());
        target.setDoi(source.getDoi());

        if (!source.getFile().isEmpty()) {
            target.setFilename(source.getFile().getOriginalFilename());
        }

        return target;
    }

    protected static <T extends Entry, U extends EntryForm> U initValues(T source, U target) {
        target.setId(source.getId());
        target.setType(source.getType());
        target.setAbstrct(source.getAbstrct());
        target.setNotes(source.getNotes());
        target.setDoi(source.getDoi());
        target.setUrl(source.getUrl());
        target.setFilename(source.getFilename());

        return target;
    }
}