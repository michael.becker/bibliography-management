package de.caterdev.bibliography.form;

import de.caterdev.bibliography.interfce.InterfaceType;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImportForm {
    private String content;
    private MultipartFile file;
    private InterfaceType type;
}
