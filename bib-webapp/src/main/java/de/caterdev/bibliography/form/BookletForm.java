package de.caterdev.bibliography.form;

import de.caterdev.bibliography.model.entry.Booklet;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookletForm extends EntryForm {
    public BookletForm() {
        setType(EntryType.Booklet);
    }

    public static BookletForm fromBooklet(Booklet booklet) {
        BookletForm entryForm = initValues(booklet, new BookletForm());

        entryForm.setTitle(booklet.getTitle());

        entryForm.setAuthors(String.join(",", booklet.getAuthors()));
        entryForm.setHowPublished(booklet.getHowPublished());
        entryForm.setAddress(booklet.getAddress());
        entryForm.setMonth(booklet.getMonth());
        entryForm.setYear(booklet.getYear());
        entryForm.setNote(booklet.getNote());

        return entryForm;
    }


    public static Booklet toBooklet(EntryForm entryForm) {
        Booklet booklet = initValues(entryForm, new Booklet());

        booklet.setTitle(entryForm.getTitle());

        booklet.setAuthors(Arrays.asList(entryForm.getAuthors().split(",")));
        booklet.setHowPublished(entryForm.getHowPublished());
        booklet.setAddress(entryForm.getAddress());
        booklet.setMonth(entryForm.getMonth());
        booklet.setYear(entryForm.getYear());
        booklet.setNote(entryForm.getNote());

        return booklet;
    }
}
