package de.caterdev.bibliography.form;

import de.caterdev.bibliography.model.entry.Article;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;

@EqualsAndHashCode(callSuper = true)
@Data
public class ArticleForm extends EntryForm {
    public ArticleForm() {
        setType(EntryType.Article);
    }

    public static ArticleForm fromArticle(Article article) {
        ArticleForm articleForm = initValues(article, new ArticleForm());

        articleForm.setAuthors(String.join(",", article.getAuthors()));
        articleForm.setTitle(article.getTitle());
        articleForm.setJournal(article.getJournal());
        articleForm.setYear(article.getYear());
        articleForm.setVolume(article.getVolume());
        articleForm.setNumber(article.getNumber());
        articleForm.setPages(article.getPages());
        articleForm.setMonth(article.getMonth());
        articleForm.setNote(article.getNote());

        return articleForm;
    }

    public static Article toArticle(EntryForm articleForm) {
        Article article = initValues(articleForm, new Article());

        article.setAuthors(Arrays.asList(articleForm.getAuthors().split(",")));
        article.setTitle(articleForm.getTitle());
        article.setJournal(articleForm.getJournal());
        article.setYear(articleForm.getYear());
        article.setVolume(articleForm.getVolume());
        article.setNumber(articleForm.getNumber());
        article.setPages(articleForm.getPages());
        article.setMonth(articleForm.getMonth());
        article.setNote(articleForm.getNote());

        return article;
    }
}
