package de.caterdev.bibliography;

import de.caterdev.bibliography.interfce.bib.BibExporter;
import de.caterdev.bibliography.interfce.ris.RISImporter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class BibWebApp {

    public static void main(String[] args) {
        SpringApplication.run(BibWebApp.class, args);
    }

    @Bean
    public RISImporter risImporter() {
        return new RISImporter();
    }

    @Bean
    public BibExporter bibExporter() {
        return new BibExporter();
    }
}
