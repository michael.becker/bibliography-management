/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.test.interfce.bib;

import de.caterdev.bibliography.interfce.bib.BibExporter;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.test.interfce.Scenario;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BibExporterTest {
    private final BibExporter exporter = new BibExporter();

    @Test
    void testCreateBibKeysForSingleEntries() {
        assertEquals("Cohen1963", BibExporter.createBibKey(Scenario.article));
        assertEquals("Susskind2014", BibExporter.createBibKey(Scenario.book));
        assertEquals("Urry2016", BibExporter.createBibKey(Scenario.inBook));
        assertEquals("Shapiro2018", BibExporter.createBibKey(Scenario.inCollection));
        assertEquals("Holleis2010", BibExporter.createBibKey(Scenario.inProceedings));
    }

    @Test
    void testCreateBibKeysForDatabase() {
        Map<Entry, String> keys = BibExporter.createBibKeys(Scenario.smallDatabase);

        assertEquals(2, keys.size());
        assertTrue(keys.containsValue("Cohen1963"));
        assertTrue(keys.containsValue("Cohen1963a"));
    }

    @Test
    void testExportArticle() {
        String[] actualLines = exporter.export(Scenario.article).split("\\R+");
        String[] expectedLines = Scenario.getArticleBibContentLines();

        compareLines(expectedLines, actualLines);
    }

    @Test
    void testExportBook() {
        String[] expectedLines = Scenario.getBookBibContentLines();
        String[] actualLines = exporter.export(Scenario.book).split("\\R+");

        compareLines(expectedLines, actualLines);
    }

    @Test
    void testExportInBook() {
        String[] expectedLines = Scenario.getInBookBibContentLines();
        String[] actualLines = exporter.export(Scenario.inBook).split("\\R+");

        compareLines(expectedLines, actualLines);
    }

    @Test
    void testExportInCollection() {
        String[] expectedLines = Scenario.getInCollectionBibContentLines();
        String[] actualLines = exporter.export(Scenario.inCollection).split("\\R+");

        compareLines(expectedLines, actualLines);
    }

    @Test
    void testExportInProceedings() {
        String[] expectedLines = Scenario.getInProceedingsContentLines();
        String[] actualLines = exporter.export(Scenario.inProceedings).split("\\R+");

        compareLines(expectedLines, actualLines);
    }

    @Test
    void testExportDatabase() {
        String[] expectedLines = Scenario.getSmallDatabaseContentLines();
        String[] actualLines = exporter.export(Scenario.smallDatabase).split("\\R+");

        compareLines(expectedLines, actualLines);

        expectedLines = Scenario.getDatabaseContentLines();
        actualLines = exporter.export(Scenario.database).split("\\R+");

        compareLines(expectedLines, actualLines);
    }

    /**
     * Compares the lines of the expected bib file with the ones given in the produced bib file.
     *
     * @param expectedLines the lines in the expected bib file
     * @param actualLines   the lines of the actual bib file
     */
    private void compareLines(String[] expectedLines, String[] actualLines) {
        assertEquals(expectedLines.length, actualLines.length, "number of lines in " + Arrays.toString(actualLines) + " does not match expexted " + Arrays.toString(expectedLines));

        Map<String, String> expectedEntries = buildMappedLines(expectedLines);
        Map<String, String> actualEntries = buildMappedLines(actualLines);

        // we first check that all entries of the expected bib file are represented in the actual bib file
        for (String key : expectedEntries.keySet()) {
            assertTrue(actualEntries.containsKey(key), key + " not contained in entries " + actualEntries);
            assertEquals(expectedEntries.get(key), actualEntries.get(key));
        }

        // second, we check that the actual bib file does not contain additional field
        // should be not the case due to the length predicate above but included for completeness reasons)
        for (String key : actualEntries.keySet()) {
            assertTrue(expectedEntries.containsKey(key));
        }
    }

    /**
     * Builds mapped bib entries from the given lines. The mapping contains the followin fields:
     * <ul>
     *     <li>__begin: represents the beginning of the bibTex entry, e.g. <pre>@Article{bibId</pre></li>
     *     <li>__end: contains the end of the bibTex entry, usualy <pre>}</pre></li>
     *     <li>$bibkey: contains the line of the bibkey, e.g. <pre>volume={50}</pre></li>
     * </ul>
     *
     * @param lines the lines to parse
     * @return a map containing all the lines with their respective bibText field type
     */
    private Map<String, String> buildMappedLines(String[] lines) {
        Map<String, String> lineEntries = new HashMap<>(lines.length);
        for (String line : lines) {
            String expectedLine = line.replaceAll("\\s", "");
            if (expectedLine.startsWith("@")) {
                lineEntries.put("__begin", expectedLine);
            } else if (expectedLine.startsWith("}")) {
                lineEntries.put("__end", expectedLine);
            } else {
                String key = expectedLine.substring(0, expectedLine.indexOf("="));
                lineEntries.put(key, expectedLine);
            }
        }

        return lineEntries;
    }

}
