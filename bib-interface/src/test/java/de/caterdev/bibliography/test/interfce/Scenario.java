/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.test.interfce;

import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.model.entry.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Year;
import java.util.Arrays;
import java.util.Collections;

public class Scenario {
    public static final Article article = new Article();
    public static final Article article2 = new Article();
    public static final Book book = new Book();
    public static final InBook inBook = new InBook();
    public static final InCollection inCollection = new InCollection();
    public static final InProceedings inProceedings = new InProceedings();
    public static final BibDatabase database = new BibDatabase();
    public static final BibDatabase smallDatabase = new BibDatabase();

    static {
        article.setAuthors(Collections.singletonList("P. J. Cohen"));
        article.setTitle("The independence of the continuum hypothesis");
        article.setJournal("Proceedings of the National Academy of Sciences");
        article.setVolume("50");
        article.setNumber("6");
        article.setYear(Year.parse("1963"));
        article.setPages("1143--1148");
    }

    static {
        article2.setAuthors(Collections.singletonList("P. J. Cohen"));
        article2.setTitle("The independence of the continuum hypothesis");
        article2.setJournal("Proceedings of the National Academy of Sciences");
        article2.setVolume("50");
        article2.setNumber("6");
        article2.setYear(Year.parse("1963"));
        article2.setPages("1143--1148");
    }

    static {
        book.setAuthors(Arrays.asList("Leonard Susskind", "George Hrabovsky"));
        book.setPublisher("Penguin Random House");
        book.setTitle("Classical mechanics: the theoretical minimum");
        book.setYear(Year.parse("2014"));
        book.setAddress("New York, NY");
    }

    static {
        inBook.setAuthors(Arrays.asList("Lisa A. Urry", "Michael L. Cain", "Steven A. Wasserman", "Peter V. Minorsky", "Jane B. Reece"));
        inBook.setTitle("Photosynthesis");
        inBook.setBookTitle("Campbell Biology");
        inBook.setYear(Year.parse("2016"));
        inBook.setPublisher("Pearson");
        inBook.setAddress("New York, NY");
        inBook.setPages("187--221");
    }

    static {
        inCollection.setAuthors(Collections.singletonList("Howard M. Shapiro"));
        inCollection.setEditors(Arrays.asList("Hawley, Teresa S.", "Hawley, Robert G."));
        inCollection.setTitle("Flow Cytometry: The Glass Is Half Full");
        inCollection.setBookTitle("Flow Cytometry Protocols");
        inCollection.setYear(Year.parse("2018"));
        inCollection.setPublisher("Springer");
        inCollection.setAddress("New York, NY");
        inCollection.setPages("1--10");
    }

    static {
        inProceedings.setAuthors(Arrays.asList("Paul Holleis", "Matthias Wagner", "Johan Koolwaaij"));
        inProceedings.setBookTitle("Proc. of the 6th Nordic Conf. on Human-Computer Interaction");
        inProceedings.setTitle("Studying mobile context-aware social services in the wild");
        inProceedings.setYear(Year.parse("2010"));
        inProceedings.setAddress("New York, NY");
        inProceedings.setPages("207--216");
        inProceedings.setPublisher("ACM");
        inProceedings.setSeries("NordiCHI");
    }

    static {
        database.setName("default database");
        database.addEntries(article, article2, book, inBook, inCollection, inProceedings);
    }

    static {
        smallDatabase.setName("small database");
        smallDatabase.addEntries(article, article2);
    }

    public static String getArticleBibContent() {
        return getFileContents("/bib-scenario/entry-article.bib");
    }

    public static String[] getArticleBibContentLines() {
        return getArticleBibContent().split("\\R+");
    }

    public static String getBookBibContent() {
        return getFileContents("/bib-scenario/entry-book.bib");
    }

    public static String[] getBookBibContentLines() {
        return getBookBibContent().split("\\R+");
    }

    public static String getInBookBibContent() {
        return getFileContents("/bib-scenario/entry-inbook.bib");
    }

    public static String[] getInBookBibContentLines() {
        return getInBookBibContent().split("\\R+");
    }

    public static String getInCollectionBibContent() {
        return getFileContents("/bib-scenario/entry-incollection.bib");
    }

    public static String[] getInCollectionBibContentLines() {
        return getInCollectionBibContent().split("\\R+");
    }

    public static String getInProceedingsContent() {
        return getFileContents("/bib-scenario/entry-inproceedings.bib");
    }

    public static String[] getInProceedingsContentLines() {
        return getInProceedingsContent().split("\\R+");
    }

    public static String getDatabaseContent() {
        return getFileContents("/bib-scenario/database.bib");
    }

    public static String[] getDatabaseContentLines() {
        return getDatabaseContent().split("\\R+");
    }

    public static String getSmallDatabaseContent() {
        return getFileContents("/bib-scenario/small-database.bib");
    }

    public static String[] getSmallDatabaseContentLines() {
        return getSmallDatabaseContent().split("\\R+");
    }


    private static String getFileContents(String filename) {
        try {
            return Files.readString(Path.of(Scenario.class.getResource(filename).toURI()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
