package de.caterdev.bibliography.test.interfce.imprt;

import de.caterdev.bibliography.interfce.ris.RISImporter;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RISImporterTest {
    private final RISImporter risImporter = new RISImporter();

    @Test
    void testImportArticle() throws Exception {
        URL file = getClass().getResource("/ris-testfile.ris");
        String content = new String(Files.readAllBytes(Path.of(file.toURI())));

        Entry entry = risImporter.parse(content);
        assertNotNull(entry);
        assertEquals(EntryType.Article, entry.getType());
        assertEquals("10.1007/s10824-019-09358-z", entry.getDoi());
    }
}
