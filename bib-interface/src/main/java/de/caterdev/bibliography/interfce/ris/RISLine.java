/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.interfce.ris;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents a single line in a {@link RISFile}.
 */
@AllArgsConstructor
@Getter
public class RISLine {
    private final RISField field;
    private final String content;

    /**
     * Creates a new line from the given string source. The given line must adhere to the following constraints:
     * <ul>
     *     character 1, character 2: type of the field (see {@link RISField})
     *     character 3, character 4: whitespace
     *     character 5: dash
     *     character 6: whitespace
     *     character 7+: content of the field
     * </ul>
     *
     * @param line the line to parse
     */
    public RISLine(String line) {
        String[] lineContent = {line.substring(0, 6), line.substring(6)};

        this.field = RISField.parse(lineContent[0]);
        this.content = lineContent[1];
    }

    @Override
    public String toString() {
        return field.getFieldName() + content;
    }
}
