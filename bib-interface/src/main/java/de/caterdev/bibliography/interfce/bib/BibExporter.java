/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.interfce.bib;

import de.caterdev.bibliography.interfce.EntryExporter;
import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.database.BibDatabase;
import de.caterdev.bibliography.model.entry.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides methods for exporting entries into BibTeX format.
 */
public class BibExporter implements EntryExporter {
    @Override
    public String export(Entry entry) {
        return switch (entry.getType()) {
            case Article -> exportArticle((Article) entry);
            case Book -> exportBook((Book) entry);
            case InBook -> exportInBook((InBook) entry);
            case InCollection -> exportInCollection((InCollection) entry);
            case InProceedings -> exportInProceedings((InProceedings) entry);
            default -> throw new RuntimeException("Type " + entry.getType() + " not supported");
        };
    }

    public String export(Entry entry, String bibKey) {
        return switch (entry.getType()) {
            case Article -> exportArticle((Article) entry, bibKey);
            case Book -> exportBook((Book) entry, bibKey);
            case InBook -> exportInBook((InBook) entry, bibKey);
            case InCollection -> exportInCollection((InCollection) entry, bibKey);
            case InProceedings -> exportInProceedings((InProceedings) entry, bibKey);
            default -> throw new RuntimeException("Type " + entry.getType() + " not supported");
        };
    }

    @Override
    public String export(BibDatabase database) {
        Map<Entry, String> bibKeys = createBibKeys(database);
        StringBuilder result = new StringBuilder();

        for (Entry entry : database.getEntries()) {
            String exportedEntry = export(entry, bibKeys.get(entry));

            result.append(exportedEntry).append("\n\n");
        }

        return result.toString();
    }

    public static <U extends HasAuthors & HasYear> String createBibKey(U entry) {
        return resolveBibKeyPrefix(entry) + resolveBibKeyPostfix(entry);
    }

    public static Map<Entry, String> createBibKeys(BibDatabase database) {
        Map<Entry, String> bibKeys = new HashMap<>(database.getEntries().size());
        Map<String, Integer> keys = new HashMap<>(database.getEntries().size());
        //List<Entry> entries = new ArrayList<>(database.getEntries().values());


        for (Entry entry : database.getEntries()) {
            if (entry instanceof HasAuthors && entry instanceof HasYear) {
                String prefix = resolveBibKeyPrefix((HasAuthors) entry);
                String postfix = resolveBibKeyPostfix((HasYear) entry);
                String key = prefix + postfix;

                // check if the given key is already used
                // if not: use it as bibtex key
                // is used: increase the counter and use the bibtex key key+(a,b,c,...)
                if (keys.containsKey(key)) {
                    Integer value = keys.get(key);
                    keys.put(key, value + 1);
                    key = key + (char) (value + 97 - 1); // ascii a=97, decrease by one to start with a
                } else {
                    keys.put(key, 1);
                }

                bibKeys.put(entry, key);
            } else {
                throw new IllegalArgumentException(entry.getClass() + " not supported");
            }
        }

        return bibKeys;
    }

    private static <U extends HasAuthors> String resolveBibKeyPrefix(U entry) {
        String prefix;
        if (entry.getAuthors().isEmpty()) {
            prefix = "Unknown";
        } else {
            String author = entry.getAuthors().get(0);
            prefix = author.substring(author.lastIndexOf(" ")).trim();
        }

        return prefix;
    }

    private static <U extends HasYear> String resolveBibKeyPostfix(U entry) {
        return entry.getYear().toString();
    }

    private String exportArticle(Article article) {
        return exportArticle(article, createBibKey(article));
    }

    private String exportArticle(Article article, String bibKey) {
        StringBuilder result = new StringBuilder("@Article{");
        result.append(bibKey).append(",\n");
        result.append(getAuthors(article));
        result.append(getTitle(article));
        result.append(getJournal(article));
        result.append(getYear(article));
        result.append(getPages(article));
        result.append("}");

        return result.toString();
    }

    private String exportBook(Book book) {
        return exportBook(book, createBibKey(book));
    }

    private String exportBook(Book book, String bibKey) {
        StringBuilder result = new StringBuilder("@Book{");
        result.append(bibKey).append(",\n");
        result.append(getAuthors(book));
        result.append(getPublisher(book));
        result.append(getTitle(book));
        result.append(getYear(book));
        result.append(getEdition(book));
        result.append(getAddress(book));
        result.append("}");

        return result.toString();
    }

    private String exportInBook(InBook entry) {
        return exportInBook(entry, createBibKey(entry));
    }

    private String exportInBook(InBook entry, String bibKey) {
        StringBuilder result = new StringBuilder("@InBook{");
        result.append(bibKey).append(",\n");
        result.append(getAuthors(entry));
        result.append(getTitle(entry));
        result.append(getBookTitle(entry));
        result.append(getYear(entry));
        result.append(getPublisher(entry));
        result.append(getAddress(entry));
        result.append(getPages(entry));
        result.append("}");

        return result.toString();
    }

    private String exportInCollection(InCollection entry) {
        return exportInCollection(entry, createBibKey(entry));
    }

    private String exportInCollection(InCollection entry, String bibKey) {
        StringBuilder result = new StringBuilder("@InCollection{");
        result.append(bibKey).append(",\n");
        result.append(getAuthors(entry));
        result.append(getEditors(entry));
        result.append(getTitle(entry));
        result.append(getBookTitle(entry));
        result.append(getYear(entry));
        result.append(getPublisher(entry));
        result.append(getAddress(entry));
        result.append(getPages(entry));
        result.append("}");

        return result.toString();
    }

    private String exportInProceedings(InProceedings inProceedings) {
        return exportInProceedings(inProceedings, createBibKey(inProceedings));
    }

    private String exportInProceedings(InProceedings inProceedings, String bibKey) {
        StringBuilder result = new StringBuilder("@InProceedings{");
        result.append(bibKey).append(",\n");
        result.append(getAuthors(inProceedings));
        result.append(getBookTitle(inProceedings));
        result.append(getTitle(inProceedings));
        result.append(getYear(inProceedings));
        result.append(getAddress(inProceedings));
        result.append(getPages(inProceedings));
        result.append(getPublisher(inProceedings));
        result.append(getSeries(inProceedings));
        result.append("}");

        return result.toString();
    }

    private String getAddress(HasAddress entry) {
        return null != entry.getAddress() ? getField("address", entry.getAddress()) : "";
    }

    private String getAuthors(HasAuthors entry) {
        return !entry.getAuthors().isEmpty() ? getField("author", String.join(" and ", entry.getAuthors())) : "";
    }

    private String getBookTitle(HasBookTitle entry) {
        return null != entry.getBookTitle() ? getField("booktitle", entry.getBookTitle()) : "";
    }

    private String getEdition(HasEdition entry) {
        return null != entry.getEdition() ? getField("edition", entry.getEdition()) : "";
    }

    private String getEditors(HasEditors entry) {
        return !entry.getEditors().isEmpty() ? getField("editor", String.join(" and ", entry.getEditors())) : "";
    }

    private String getPages(HasPages entry) {
        return null != entry.getPages() ? getField("pages", entry.getPages()) : "";
    }

    private String getPublisher(HasPublisher entry) {
        return null != entry.getPublisher() ? getField("publisher", entry.getPublisher()) : "";
    }

    private String getSeries(HasSeries entry) {
        return null != entry.getSeries() ? getField("series", entry.getSeries()) : "";
    }

    private String getTitle(HasTitle entry) {
        return null != entry.getTitle() ? getField("title", entry.getTitle()) : "";
    }

    private String getJournal(HasJournal entry) {
        String journal = null != entry.getJournal() ? getField("journal", entry.getJournal()) : "";
        String volume = null != entry.getVolume() ? getField("volume", entry.getVolume()) : "";
        String number = null != entry.getNumber() ? getField("number", entry.getNumber()) : "";

        return journal + volume + number;
    }

    private String getYear(HasYear entry) {
        return null != entry.getYear() ? getField("year", entry.getYear().toString()) : "";
    }

    private String getField(String fieldname, String value) {
        return String.format("\t%s\t= {%s},\n", fieldname, value);
    }
}
