/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.interfce.ris;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a RIS file consisting of several {@link RISLine}s.
 */
@Data
@ToString
public class RISFile {
    private final List<RISLine> lines = new ArrayList<>();
    private final RISType type;

    public void addLine(RISLine line) {
        lines.add(line);
    }

    /**
     * Creates a new RIS file from the given source string
     *
     * @param source the string to parse
     */
    public RISFile(String source) {
        // split by \R+: linebreak matcher + treat multiple empty lines as single separator (https://stackoverflow.com/questions/454908/split-java-string-by-new-line)
        String[] lines = source.split("\\R+");

        RISLine first = new RISLine(lines[0]);

        if (first.getField() != RISField.Type) {
            throw new RuntimeException("First line in RIS must be " + RISField.Type + " but is " + lines[0]);
        }

        RISLine last = new RISLine(lines[lines.length - 1]);

        if (last.getField() != RISField.END) {
            throw new RuntimeException("Last line in RIS must be " + RISField.END + " but is " + lines[lines.length - 1]);
        }

        addLine(first);
        for (int i = 1; i < lines.length - 1; i++) {
            addLine(new RISLine(lines[i]));
        }
        addLine(last);

        this.type = RISType.parse(first.getContent());
    }

    /**
     * Exportes this RIS file into a RIS string
     *
     * @return the RIS string represented by this RIS file
     */
    public String export() {
        StringBuilder builder = new StringBuilder();
        for (RISLine line : lines) {
            builder.append(line).append("\n");
        }

        return builder.toString();
    }
}
