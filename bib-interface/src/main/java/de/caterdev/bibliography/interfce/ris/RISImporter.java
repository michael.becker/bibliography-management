/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.interfce.ris;

import de.caterdev.bibliography.interfce.EntryImporter;
import de.caterdev.bibliography.model.entry.Article;
import de.caterdev.bibliography.model.entry.Book;
import de.caterdev.bibliography.model.Entry;

import java.time.Year;

/**
 * Provides methods for importing RIS files.
 * <p>
 * A RIS file consists of line-based information about a bib entry. Each line has a specific type and content. For additional details about the specification see <a href="https://en.wikipedia.org/wiki/RIS_(file_format)">https://en.wikipedia.org/wiki/RIS_(file_format)</a>
 * </p>
 *
 * <p>
 * Currently, we only support single-entry RIS files.
 * </p>
 */
public class RISImporter extends EntryImporter {
    @Override
    public Entry parse(String source) {
        RISFile risFile = new RISFile(source);

        return switch (risFile.getType()) {
            case Book -> parseBook(risFile);
            case Journal -> parseArticle(risFile);
            default -> throw new IllegalArgumentException("Entry type " + risFile.getType() + " currently not supported.");
        };
    }

    /**
     * Parses a given RIS file into a book entry.
     *
     * @param risFile the RIS file to parse
     * @return the book entry
     */
    private Book parseBook(RISFile risFile) {
        Book book = new Book();
        for (RISLine risLine : risFile.getLines()) {
            RISField field = risLine.getField();
            String content = risLine.getContent();

            switch (field) {
                case PublicationYear -> book.setYear(Year.parse(content));
                case URL -> book.setUrl(content);
                case DOI -> book.setDoi(content);
                case Title -> book.setTitle(content);
                case Author -> book.getAuthors().add(content);
                case Abstract -> book.setAbstrct(content);
                case Publisher -> book.setPublisher(content);
            }
        }
        return null;
    }

    /**
     * Parses a given RIS file into an article entry.
     *
     * @param risFile the RIS file to parse
     * @return the article entry
     */
    private Article parseArticle(RISFile risFile) {
        Article article = new Article();
        for (RISLine risLine : risFile.getLines()) {
            RISField field = risLine.getField();
            String content = risLine.getContent();

            switch (field) {
                case Author -> article.getAuthors().add(content);
                case Title -> article.setTitle(content);
                case Journal -> article.setJournal(content);
                case DOI -> article.setDoi(content);
                case URL -> article.setUrl(content);
                case Volume -> article.setVolume(content);
                case Issue -> article.setNumber(content);
                case PublicationYear -> article.setYear(Year.parse(content));
                case Abstract -> article.setAbstrct(content);
            }
        }

        return article;
    }
}
