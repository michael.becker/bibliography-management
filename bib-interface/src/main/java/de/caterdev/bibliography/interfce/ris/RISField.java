/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.interfce.ris;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Collection of possible field types in a RIS file. Each field has the format field type, whitespace, whitespace, dash, whitespace.
 */
@AllArgsConstructor
@Getter
public enum RISField {
    Type("TY  - "),
    Author("AU  - "),
    PublicationYear("PY  - "),
    Date("DA  - "),
    Title("TI  - "),
    Journal("JO  - "),
    StartPage("SP  - "),
    EndPage("EP  - "),
    Volume("VL  - "),
    Issue("IS  - "),
    Publisher("PB  - "),
    Abstract("AB  - "),
    ISSN("SN  - "),
    URL("UR  - "),
    DOI("DO  - "),
    ID("ID  - "),
    END("ER  - ");

    private final String fieldName;

    public static RISField parse(String text) {
        for (RISField field : values()) {
            if (field.fieldName.equals(text)) {
                return field;
            }
        }

        throw new IllegalArgumentException(text + " could not be parsed to RIS field");
    }
}
