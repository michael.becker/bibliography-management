package de.caterdev.bibliography.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Data
@Entity
public abstract class Entry {
    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();
    @Lob
    private String abstrct;
    private String notes;
    private String doi;
    private String url;
    @OneToMany
    private Collection<EntryFile> entryFiles;
    @Deprecated
    private String filename;
    @ElementCollection
    private Collection<String> keywords = new ArrayList<>();
    @ManyToMany
    private Set<EntryGroup> entryGroups = new HashSet<>();

    public abstract EntryType getType();
}
