/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.Month;
import java.time.Year;
import java.util.List;

/**
 * An institutionally published report such as a report from a school, a government organization, an organization, or a company. This entry type is also frequently used for white papers and working papers.
 *
 * <pre>
 *     &#64;techreport{CitekeyTechreport,
 *       title       = "{W}asatch {S}olar {P}roject Final Report",
 *       author      = "Bennett, Vicki and Bowman, Kate and Wright, Sarah",
 *       institution = "Salt Lake City Corporation",
 *       address     = "Salt Lake City, UT",
 *       number      = "DOE-SLC-6903-1",
 *       year        = 2018,
 *       month       = sep
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class TechReport extends Entry implements HasAuthors, HasTitle, HasInstitution, HasAddress, HasNumber, HasYear, HasMonth {
    @ElementCollection
    private List<String> authors;
    private String title;
    private String institution;
    private String address;
    private String number;
    private Year year;
    private Month month;

    @Override
    public EntryType getType() {
        return EntryType.TechReport;
    }
}
