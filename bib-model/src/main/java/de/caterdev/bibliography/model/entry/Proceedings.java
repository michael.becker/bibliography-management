/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.Year;
import java.util.Collection;

/**
 * A conference proceeding.
 *
 * <pre>
 *     &#64;proceedings{CitekeyProceedings,
 *       editor    = "Susan Stepney and Sergey Verlan",
 *       title     = "Proceedings of the 17th International Conference on Computation and Natural Computation, Fontainebleau, France",
 *       series    = "Lecture Notes in Computer Science",
 *       volume    = "10867",
 *       publisher = "Springer",
 *       address   = "Cham, Switzerland",
 *       year      = 2018
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Proceedings extends Entry implements HasEditors, HasTitle, HasSeries, HasVolume, HasPublisher, HasAddress, HasYear {
    @ElementCollection
    private Collection<String> editors;
    private String title;
    private String series;
    private String volume;
    private String publisher;
    private String address;
    private Year year;

    @Override
    public EntryType getType() {
        return EntryType.Proceedings;
    }
}
