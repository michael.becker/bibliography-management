/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 * An article from a journal, magazine, newspaper, or periodical.
 *
 * <pre>
 *      &#64;article{CitekeyArticle,
 *       author   = "P. J. Cohen",
 *       title    = "The independence of the continuum hypothesis",
 *       journal  = "Proceedings of the National Academy of Sciences",
 *       year     = 1963,
 *       volume   = "50",
 *       number   = "6",
 *       pages    = "1143--1148",
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@Entity
public class Article extends Entry implements HasAuthors, HasTitle, HasYear, HasJournal, HasMonth {
    @ElementCollection
    @NotEmpty
    private List<String> authors = new ArrayList<>();
    @NotBlank
    private String title;
    @NotBlank
    private String journal;
    @NotNull
    private Year year;
    private String volume;
    private String number;
    private String pages;
    private Month month;
    private String note;

    @Override
    public EntryType getType() {
        return EntryType.Article;
    }
}
