/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 * A paper that has been published in conference proceedings. The usage of conference and inproceedings is the same. The conference entry was included for Scribe compatibility.
 *
 * <pre>
 *     &#64;inproceedings{CitekeyInproceedings,
 *       author    = "Holleis, Paul and Wagner, Matthias and Koolwaaij, Johan",
 *       title     = "Studying mobile context-aware social services in the wild",
 *       booktitle = "Proc. of the 6th Nordic Conf. on Human-Computer Interaction",
 *       series    = "NordiCHI",
 *       year      = 2010,
 *       pages     = "207--216",
 *       publisher = "ACM",
 *       address   = "New York, NY"
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class InProceedings extends Entry implements HasAuthors, HasTitle, HasBookTitle, HasSeries, HasYear, HasPages, HasPublisher, HasAddress {
    @ElementCollection
    private List<String> authors = new ArrayList<>();
    private String title;
    private String bookTitle;
    private String series;
    private Year year;
    private String pages;
    private String publisher;
    private String address;

    @Override
    public EntryType getType() {
        return EntryType.InProceedings;
    }
}
