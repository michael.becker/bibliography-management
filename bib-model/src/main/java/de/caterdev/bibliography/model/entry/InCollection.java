/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.Year;
import java.util.Collection;
import java.util.List;

/**
 * A titled section of a book. Such as a short story within the larger collection of short stories that make up the book.
 *
 * <pre>
 *     &#64;incollection{CitekeyIncollection,
 *       author    = "Shapiro, Howard M.",
 *       editor    = "Hawley, Teresa S. and Hawley, Robert G.",
 *       title     = "Flow Cytometry: The Glass Is Half Full",
 *       booktitle = "Flow Cytometry Protocols",
 *       year      = 2018,
 *       publisher = "Springer",
 *       address   = "New York, NY",
 *       pages     = "1--10"
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class InCollection extends Entry implements HasAuthors, HasEditors, HasTitle, HasBookTitle, HasYear, HasPublisher, HasAddress, HasPages {
    @ElementCollection
    private List<String> authors;
    @ElementCollection
    private Collection<String> editors;
    private String title;
    private String bookTitle;
    private Year year;
    private String publisher;
    private String address;
    private String pages;

    @Override
    public EntryType getType() {
        return EntryType.InCollection;
    }
}
