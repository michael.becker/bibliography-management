/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.Year;
import java.util.List;

/**
 * A section, such as a chapter, or a page range within a book.
 *
 * <pre>
 *     &#64;inbook{CitekeyInbook,
 *       author    = "Lisa A. Urry and Michael L. Cain and Steven A. Wasserman and Peter V. Minorsky and Jane B. Reece",
 *       title     = "Photosynthesis",
 *       booktitle = "Campbell Biology",
 *       year      = "2016",
 *       publisher = "Pearson",
 *       address   = "New York, NY",
 *       pages     = "187--221"
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class InBook extends Entry implements HasAuthors, HasTitle, HasBookTitle, HasYear, HasPublisher, HasAddress, HasPages {
    @ElementCollection
    private List<String> authors;
    private String title;
    private String bookTitle;
    private Year year;
    private String publisher;
    private String address;
    private String pages;

    @Override
    public EntryType getType() {
        return EntryType.InBook;
    }
}
