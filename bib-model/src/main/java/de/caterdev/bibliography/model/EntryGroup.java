package de.caterdev.bibliography.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
public class EntryGroup {
    @Id
    @GeneratedValue
    @NotNull
    private UUID id;
    @NotBlank
    private String name;
    private String description;
    @ManyToMany
    private Set<Entry> entries = new HashSet<>();
}
