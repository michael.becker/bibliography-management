/*
 * MIT License
 *
 * Copyright (c) 2020 Michael Becker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.caterdev.bibliography.model.entry;

import de.caterdev.bibliography.model.Entry;
import de.caterdev.bibliography.model.EntryType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Month;
import java.time.Year;
import java.util.List;

/**
 * A book where the publisher is clearly identifiable.
 *
 * <pre>
 *     &#64;book{CitekeyBook,
 *       author    = "Leonard Susskind and George Hrabovsky",
 *       title     = "Classical mechanics: the theoretical minimum",
 *       publisher = "Penguin Random House",
 *       address   = "New York, NY",
 *       year      = 2014
 *     }
 * </pre>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@Entity
public class Book extends Entry implements HasAuthors, HasTitle, HasPublisher, HasYear, HasVolume, HasSeries, HasEdition, HasAddress, HasMonth {
    @ElementCollection
    @NotEmpty
    private List<String> authors;
    @NotBlank
    private String title;
    @NotBlank
    private String publisher;
    @NotNull
    private Year year;
    private String volume;
    private String series;
    private String edition;
    private String address;
    private Month month;
    private String note;

    @Override
    public EntryType getType() {
        return EntryType.Book;
    }
}
