package de.caterdev.bibliography.repo;

import de.caterdev.bibliography.model.EntryGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<EntryGroup, UUID> {
}
